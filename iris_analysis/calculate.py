from statistics import mean, median, stdev


def base_statistics(data):
    """
    Zlcza średnią, medianę i odchylenie standardowe dla wartości w każdej kolumnie. Pomija kolumny, w których znajdują się wartości tekstowe.
    """

    results = {'values': [], 'header': [], 'nrows': 1}

    for i, col in enumerate(data['header']):
        if type(data['values'][i][0]) is str:
            pass  # pomijamy kolumny tekstowe
        else:
            results['header'].append(str(col) + '.mean')
            results['values'].append(mean(data['values'][i]))
            results['header'].append(str(col) + '.median')
            results['values'].append(median(data['values'][i]))
            results['header'].append(str(col) + '.std')
            results['values'].append(stdev(data['values'][i]))
    return results
