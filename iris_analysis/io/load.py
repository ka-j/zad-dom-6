import csv


def read_csv(filename):
    """
    Czyta plik csv i zwraca słownik z wartościami z każdej kolumny.
    """

    with open(filename, 'r', newline='') as file:
        data = {'values': []}
        reader = csv.reader(file, doublequote=True, quoting=csv.QUOTE_NONNUMERIC)
        for row in reader:
            if 'header' not in data.keys():
                data['header'] = []
                for col in row:
                    data['header'].append(col)
                    data['values'].append([])
            else:
                for i, col in enumerate(row):
                    data['values'][i].append(col)
    data['nrows'] = len(data['values'][0])
    return data
