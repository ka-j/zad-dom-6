import csv


def save_csv(filename, data):
    """
    Zapisuje dane w pliku csv o podanej nazwie.
    """
    with open(filename, 'w', newline='') as file:
        writer = csv.writer(file, doublequote=True)
        writer.writerow(data['header'])
        if data['nrows'] == 1:
            writer.writerow(data['values'])
        else:
            for i in range(data['nrows']):
                row = []
                for col in data['values']:
                    row.append(col[i])
                writer.writerow(row)
