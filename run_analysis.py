import argparse

from iris_analysis import calculate
from iris_analysis.io import load, save

parser = argparse.ArgumentParser(
    description="Program, który liczy podstawowe statystyki dla danych w podanym pliku CSV i zapisuje wyniki w innym pliku CSV. Pierwsza linijka pliku wejściowego CSV powinna zawierać nagłówki.")
parser.add_argument('input_filename', metavar='i', help='Plik CSV z danymi.')
parser.add_argument('output_filename', metavar='o', help='Plik CSV, do którego mają zapisać się wyniki.')

if __name__ == "__main__":
    args = vars(parser.parse_args())
    input_filename = args['input_filename']
    output_filename = args['output_filename']

    data = load.read_csv(input_filename)

    results = calculate.base_statistics(data)

    save.save_csv(output_filename, results)
